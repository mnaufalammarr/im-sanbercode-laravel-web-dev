<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[HomeController::class,'index']);
Route::get('/register',[AuthController::class,'register']);
Route::post('/signUp',[AuthController::class,'signUp']);
Route::get('/table',function(){
    return view('table');
});
Route::get('/datatable',function(){
    return view('datatable');
});

//cast
Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/create',[CastController::class,'create']);
Route::get('/cast/{id}/edit',[CastController::class,'edit']);

//read
Route::get('/cast/{id}',[CastController::class,'show']);
//create
Route::post('/cast',[CastController::class,'store']);
//update
Route::put('/cast/{id}',[CastController::class,'update']);
//delete
Route::delete('/cast/{id}',[CastController::class,'destroy']);