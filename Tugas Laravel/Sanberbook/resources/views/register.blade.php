@extends('layouts.master')
@section('judul')
    Biodata
@endsection
@section('content')
    <form action="/signUp" method="POST">
    @csrf
    <p>First Name:</p>
    <input type="text" name="firstname">
    <p>Last Name:</p>
    <input type="text" name="lastname">
    <p>Gender:</p>
    <input type="radio" name="male" value="Male">
    <label for="html">Male</label><br>
    <input type="radio" name="female" value="Female">
    <label for="html">Female</label><br>
    <input type="radio" name="other" value="Other">
    <label for="html">Other</label><br>
    <p>Nationality</p>
    <input list="browsers" name="browser" id="browser">
    <datalist id="browsers">
      <option value="Indonesian">
      <option value="English">
      <option value="French">
      <option value="Indian">
      <option value="Italian">
      <option value="Japanese">
    </datalist>
    <p>Language Spoken:</p>
    <input type="checkbox" name="language" value="Bahasa Indonesia">
    <label for="html">Bahasa Indonesia</label><br>
    <input type="checkbox" name="language" value="English">
    <label for="html">English</label><br>
    <input type="checkbox" name="language" value="Other">
    <label for="html">Other</label><br>
    <p>Bio:</p>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
    </form>
    @endsection