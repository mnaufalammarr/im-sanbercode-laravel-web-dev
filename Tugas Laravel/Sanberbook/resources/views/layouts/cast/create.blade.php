@extends('layouts.master')
@section('judul')
    Create Cast
@endsection
@section('content')
    <form action="/cast" method="POST" >
        @csrf
        <div class="mb-3">
            <label for="castNama" class="form-label">Nama</label>
            <input type="text" class="form-control" id="castNama" name="castNama" aria-describedby="castNama">
            @error('castNama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        </div>
        <div class="mb-3">
            <label for="castUmur" class="form-label">Umur</label>
            <input type="number" class="form-control" id="castUmur" name="castUmur" aria-describedby="castUmur">
            @error('castUmur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        </div>
        <div class="mb-3">
            <label for="castBio" class="form-label">Bio</label>
            <textarea class="form-control" id="castBio" name="castBio"></textarea>
            @error('castBio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
