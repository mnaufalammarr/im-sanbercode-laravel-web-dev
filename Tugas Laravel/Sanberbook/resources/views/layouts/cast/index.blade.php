@extends('layouts.master')
@section('judul')
     Cast
@endsection

@section('content')
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<button onclick="location.href='/cast/create'"  type="button" class="btn btn-primary" style="margin-bottom: 10px">
    Tambah Data
</button>
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col" >Actions</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($cast as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->nama}}</td>
              <td>{{$value->umur}}</td>
              <td>{{$value->bio}}</td>
              <td>
                <form action="/cast/{{$value->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                  <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                      <input type="submit" class="btn btn-danger my-1" value="Delete">
                  </form>
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse              
  </tbody>
</table>
@endsection 