<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('layouts.cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layouts.cast.show', compact('cast'));
    }

    public function create(){
        return view('layouts.cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'castNama' => 'required',
            'castUmur' => 'required|numeric',
            'castBio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["castNama"],
            "umur" => $request["castUmur"],
            "bio" => $request["castBio"],
            "created_at" => now(),
            "updated_at" => now(),
        ]);
        return redirect('/cast')->with('success', 'Data berhasil ditambahkan!');
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layouts.cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'castNama' => 'required',
            'castUmur' => 'required|numeric',
            'castBio' => 'required',
        ]);
        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["castNama"],
                "umur" => $request["castUmur"],
                "bio" => $request["castBio"],
                "updated_at" => now(),
            ]);
        return redirect('/cast')->with('success', 'Data berhasil diubah!');
    }

    public function destroy($id){
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data berhasil dihapus!');
    }
}
