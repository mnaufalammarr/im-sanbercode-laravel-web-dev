<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new animal("shaun");

echo "Name : ".$sheep->name; // "shaun"
echo "<br>";
echo "Legs : ".$sheep->legs; // 2
echo "<br>";
echo "Cold Blooded : ".$sheep->cold_blooded; // false
echo "<br>";
echo "<br>";

$frog = new Frog("Buduk");

echo "Name : ".$frog->name; // "shaun"
echo "<br>";
echo "Legs : ".$frog->legs; // 2
echo "<br>";
echo "Cold Blooded : ".$frog->cold_blooded; // false
echo "<br>";
echo "Jump : ";
$frog->jump();
echo "<br>";

$kera = new Ape("kera sakti");

echo "Name : ".$kera->name; // "shaun"
echo "<br>";
echo "Legs : ".$kera->legs; // 2
echo "<br>";
echo "Cold Blooded : ".$kera->cold_blooded; // false
echo "<br>";
echo "Yell : ";
$kera->yell();
echo "<br>";
?>